require("chromedriver");
require("geckodriver");
require("iedriver");
require("edgedriver");
const { Builder } = require("selenium-webdriver");
class Driverutil {

    async getDriver() {
        var browser = process.env.BROWSER;
        var browsername = browser == undefined ? 'chrome' : browser;
        var driver;
        switch (browsername.toUpperCase()) {
            case 'FIREFOX':
                driver = new Builder().forBrowser('firefox').build();
                break;
            case 'CHROME':
                driver = new Builder().forBrowser('chrome').build();
                break;
            case 'IE':
                driver = new Builder().forBrowser('internet explorer').build();
                break;
            case 'EDGE':
                driver = new Builder().forBrowser('MicrosoftEdge').build();
                break;

            default:
                driver = new Builder().forBrowser('chrome').build();
        }
        global.driver = driver;
        return driver;
    };
};
module.exports = new Driverutil();
