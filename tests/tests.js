const googlesearchpage = require('../pages/googlehomepage');
const searchresultspage = require('../pages/searchresultspage');
const { assert, expect } = require('chai');
const driverutil = require('../driverutil/driverutil');
require('mocha-allure-reporter');


describe('Trust&Will', function () {
    let driver;
    before(async () => {

        driver = await driverutil.getDriver();

    });

    it('Trust& Will', async function () {
        const testStep = allure.createStep("initial Step", () => { });
        await googlesearchpage.navigate();
        testStep();
        allure.severity('blocker')
        expect(false).to.equal(true);
    });

    afterEach(async () => {
        await driver.takeScreenshot().then(function (png) {
            allure.createAttachment("screenshot", function () {
                return Buffer.from(png, 'base64')
            }, 'image/png')();

        })
    });

    after(() => driver.quit());
});

