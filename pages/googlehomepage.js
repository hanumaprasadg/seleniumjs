const { By, until, Key } = require("selenium-webdriver");

class goglehomepage {

    url1 = "https://www.google.com/";
    url = "https://trustandwill.com/"

    // Locators
    searchbox = By.name('q');
    getStarted = By.xpath("(//a[@title='Get Started Today'])[2]");
    getStarted2 = By.xpath("(//a[@href='#results'])[2]");
    startMyWillBtn = By.xpath("//a[normalize-space(text())='Start My Will']");
    acceptAll = By.xpath('//button[@id="cky-btn-accept"]')

    //Methods
    async waitUntilVisible() {
        // await driver.wait(until.elementLocated(this.searchbox));
        await driver.wait(until.elementLocated(this.getStarted));
    };
    async navigate() {
        await driver.navigate().to(this.url);
        await driver.manage().window().maximize();
        await this.waitUntilVisible();
    };
    async searchfor(text) {
        await driver.findElement(this.searchbox).sendKeys(text + Key.RETURN);
    }

    async startMyWill() {
        await driver.sleep(2000)
        await driver.findElement(this.acceptAll).click();
        await driver.findElement(this.getStarted).click();
        await driver.sleep(2000)
        await driver.findElement(this.getStarted2).click();
        await driver.sleep(2000)
        await driver.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(this.startMyWillBtn));
        await driver.findElement(this.startMyWillBtn).click();

    }
};

module.exports = new goglehomepage();
