const { By, until } = require("selenium-webdriver");

class searchresultspage {
    //Locators
    linkselenium = By.partialLinkText('Selenium');

    // Methods

    async waitUntilPageLoaded() {
        await driver.wait(until.titleContains("Selenium"));
    };

    async assertlinkpresent() {
        await driver.findElement(this.linkselenium).isDisplayed();
    };
}

module.exports = new searchresultspage();